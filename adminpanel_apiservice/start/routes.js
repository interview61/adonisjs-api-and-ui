'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

// Route.on('/').render('welcome')

Route.get('/', 'Auth/LoginController.index').as('login')
Route.get('/adminpanel', 'Auth/LoginController.index').as('login')

Route.post('/adminpanel/login', 'Auth/LoginController.login').as('auth.login')

// Route.get('/adminpanel/product/dataproduct', 'Product/ProductController.dataproduct')

Route.group(() => {
    Route.get('/welcome', 'Template/TemplateController.index')
    Route.post('/logout', 'Auth/LoginController.logout').as('auth.logout')

    //--------------------
    Route.get('/product/dataproduct', 'Product/ProductController.dataproduct')
    Route.resource('/product', 'Product/ProductController')
   

}).prefix('/adminpanel').middleware(['authuser']);

//---------------------------------------------------------- no session
