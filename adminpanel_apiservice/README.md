# Adonis fullstack application

This is the fullstack boilerplate for AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Session
3. Authentication
4. Web security middleware
5. CORS
6. Edge template engine
7. Lucid ORM
8. Migrations and seeds

## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick
```

or manually clone the repo and then run `npm install`.


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```


//------------------------------------------------------ first install 
https://legacy.adonisjs.com/docs/4.1/installation

Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Unrestricted

npm i -g @adonisjs/cli

//------------------------------------------------------ make controller
adonis make:controller Session/SessionController

//------------------------------------------------------ make model and migration
adonis make:model Post --migration
adonis migration:run

//------------------------------------------------------ make seeder
adonis make:seed User
adonis seed
adonis seed --files="filename.js"

//------------------------------------------------------ vendor update or create eloqueen
https://www.npmjs.com/package/adonis-lucid-update-or-create

//------------------------------------------------------ run app
adonis serve --dev

//-------------- cloudinary
https://www.codementor.io/@amooabeebi0/image-file-upload-in-adonis-js-with-cloudinary-14klwo68tw

//-------------- carbon
https://www.npmjs.com/package/carbonjs
npm i -s carbonjs

//------------ upload file
https://legacy.adonisjs.com/docs/4.1/file-system

//----------- db transaction try catch 
https://legacy.adonisjs.com/docs/4.1/lucid#_transactions


@each(post in posts)
    {{ post.title }} 
@endeach

return response.redirect('back')