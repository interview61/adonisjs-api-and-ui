'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const axios           = use("axios");
const Env             = use('Env');

const Axioscontrol    = use('App/Controllers/Http/Axios/AxioController')
const axiosctrl       = new Axioscontrol()

class UserauthDetector {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request,response,session,auth }, next) {
    // call next to advance the request
   
    if(session.get('bearer')){

      var requp = {
          method  : 'GET',
          url     : 'getuser',
          data    : {
              token : session.get('bearer')
          }
      } 
      
      var header = {
          'Authorization' : 'Bearer '+session.get('bearer'),
          // "Content-Type"  : 'multipart/form-data',
      }
      
    var datasend = await axiosctrl.axiosheader(requp,header);
      if(datasend != 'Request failed with status code 401'){
        // return response.json(datasend)
        await next()
      }else{
        session.clear()

        session.flash({notification: 'No Authorization' })
        return response.redirect('/adminpanel')
      }
      // await next()
    }else{
      session.flash({ notification: 'Silahkan Login' })
      return response.redirect('/adminpanel') 
    }
    
  }
}

module.exports = UserauthDetector
