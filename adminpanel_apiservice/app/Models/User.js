'use strict'

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

const Carbon  = use("carbonjs");

class User extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })

    this.addTrait("@provider:Lucid/UpdateOrCreate")
    
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  mastergroup () {
    return this.hasOne('App/Models/MasterGroup', 'id', 'id')
  }

  joinjabatan () {
    return this.hasOne('App/Models/Jabatan', 'id_jabatan', 'id')
  }

  async listdata(requestall){

    const limit     = requestall.length
    const offset    = requestall.start
    const search    = requestall.search.value
    const orderclm  = parseInt(requestall.order[0].column)
    const sort      = requestall.order[0].dir

    const arrayclm = [
                      'users.fullname',
                      'users.username',
                      'users.email',
                      'users.phone',
                      'master_groups.name',
                      'users.active',
                      'jabatans.name as jabatan',
                      'users.id',
                      'users.id_groups',
                    ]

    const mqdata    = await User.query()
                    .join('master_groups','users.id_groups','master_groups.id')
                    .join('jabatans','users.id_jabatan','jabatans.id')
                    .orderBy(arrayclm[orderclm],sort)
                    .whereNull('users.deleted_at')
                    .where(function(searchdata){
                      searchdata
                      .where(arrayclm[0], 'like', '%' + search + '%')
                      .orWhere(arrayclm[1], 'like', '%' + search + '%')
                      .orWhere(arrayclm[2], 'like', '%' + search + '%')
                      .orWhere(arrayclm[3], 'like', '%' + search + '%')
                      .orWhere(arrayclm[4], 'like', '%' + search + '%')
                      .orWhere(arrayclm[5], 'like', '%' + search + '%')
                      .orWhere('jabatans.name', 'like', '%' + search + '%')
                    })
                    .select(arrayclm)
                    .limit(limit)
                    .offset(offset)
                    .fetch()

    const mqdatacount = await User.query()
                      .join('master_groups','users.id_groups','master_groups.id')
                      .join('jabatans','users.id_jabatan','jabatans.id')
                      .whereNull('users.deleted_at')
                      .where(function(searchdata){
                        searchdata
                        .where(arrayclm[0], 'like', '%' + search + '%')
                        .orWhere(arrayclm[1], 'like', '%' + search + '%')
                        .orWhere(arrayclm[2], 'like', '%' + search + '%')
                        .orWhere(arrayclm[3], 'like', '%' + search + '%')
                        .orWhere(arrayclm[4], 'like', '%' + search + '%')
                        .orWhere(arrayclm[5], 'like', '%' + search + '%')
                        .orWhere('jabatans.name', 'like', '%' + search + '%')
                      })
                      .count(`${arrayclm[7]}`+' as total') 
                      .first()
   
    
    return {qdata:mqdata,qdatacount:mqdatacount}
  }

  async deletedata(iddata){
    let datetime  = new Date().toLocaleString('en-US', { timeZone: 'Asia/Jakarta' });
    let crbn      = Carbon.parse(datetime).format('YYYY-MM-DD HH:mm:ss')

    let proses = await User.query().where('id',iddata).update({'deleted_at':crbn,'active':'0' })

    return proses
  }

  async listdatatrash(requestall){

    const limit     = requestall.length
    const offset    = requestall.start
    const search    = requestall.search.value
    const orderclm  = parseInt(requestall.order[0].column)
    const sort      = requestall.order[0].dir

    const arrayclm = [
          'users.fullname',
          'users.username',
          'users.email',
          'users.phone',
          'master_groups.name',
          'users.active',
          'jabatans.name as jabatan',
          'users.id',
        ]

    const mqdata    = await User.query()
        .join('master_groups','users.id_groups','master_groups.id')
        .join('jabatans','users.id_jabatan','jabatans.id')
        .orderBy(arrayclm[orderclm],sort)
        .whereNotNull('users.deleted_at')
        .where(function(searchdata){
          searchdata
          .where(arrayclm[0], 'like', '%' + search + '%')
          .orWhere(arrayclm[1], 'like', '%' + search + '%')
          .orWhere(arrayclm[2], 'like', '%' + search + '%')
          .orWhere(arrayclm[3], 'like', '%' + search + '%')
          .orWhere(arrayclm[4], 'like', '%' + search + '%')
          .orWhere(arrayclm[5], 'like', '%' + search + '%')
          .orWhere('jabatans.name', 'like', '%' + search + '%')
        })
        .select(arrayclm)
        .limit(limit)
        .offset(offset)
        .fetch()

    const mqdatacount = await User.query()
          .join('master_groups','users.id_groups','master_groups.id')
          .join('jabatans','users.id_jabatan','jabatans.id')
          .whereNotNull('users.deleted_at')
          .where(function(searchdata){
            searchdata
            .where(arrayclm[0], 'like', '%' + search + '%')
            .orWhere(arrayclm[1], 'like', '%' + search + '%')
            .orWhere(arrayclm[2], 'like', '%' + search + '%')
            .orWhere(arrayclm[3], 'like', '%' + search + '%')
            .orWhere(arrayclm[4], 'like', '%' + search + '%')
            .orWhere(arrayclm[5], 'like', '%' + search + '%')
            .orWhere('jabatans.name', 'like', '%' + search + '%')
          })
          .count(`${arrayclm[7]}`+' as total') 
          .first()
   
    return {qdata:mqdata,qdatacount:mqdatacount}
  }
  

  async restoredata(iddata){
    let proses = await User.query().where('id',iddata).update({'deleted_at':null,'active':'1' })
    return proses
  }

  async store(request,dtphoto){
    let proses = await User.create({
                  fullname  : request.input('fullname'),
                  username  : request.input('username'),
                  email     : request.input('email'),
                  password  : request.input('password'),
                  phone     : request.input('phone'),
                  active    : request.input('active'),
                  id_groups : request.input('id_groups'),
                  id_jabatan: request.input('id_jabatan'),
                  photo     : dtphoto
                })
    return proses
  }

  async storemember(request,dtphoto){
    let proses = await User.create({
                  fullname  : request.input('fullname'),
                  username  : request.input('username'),
                  email     : request.input('email'),
                  password  : request.input('password'),
                  phone     : request.input('phone'),
                  active    : '1',
                  id_groups : '3',
                  id_role   : '3',
                  id_jabatan: request.input('id_jabatan'),
                  photo     : dtphoto
                })
    return proses
  }
  
  async update(request,dtphoto,idpush){
   
    if(request.input('password')){

      var proses = await User.updateOrCreate(
                  {
                      id: idpush 
                  },
                  {
                    fullname  : request.input('fullname'),
                    username  : request.input('username'),
                    email     : request.input('email'),
                    phone     : request.input('phone'),
                    password  : request.input('password'),
                    active    : request.input('active'),
                    id_groups : request.input('id_groups'),
                    id_jabatan: request.input('id_jabatan'),
                    photo     : dtphoto
                  });

    }else{
      var proses = await User.updateOrCreate(
                    {
                        id: idpush 
                    },
                    {
                      fullname  : request.input('fullname'),
                      username  : request.input('username'),
                      email     : request.input('email'),
                      phone     : request.input('phone'),
                      active    : request.input('active'),
                      id_groups : request.input('id_groups'),
                      id_jabatan: request.input('id_jabatan'),
                      photo     : dtphoto
                    });
    }
   
    return proses
  }
}

module.exports = User
