'use strict'

const axios = use("axios");
const https = use('https');
const Env   = use('Env');

class AxioController {
    async urlapidata(){
        return 'http://localhost:3399/myapps/api/'
    }
    async axiosnoheader(req){

        var urldata = await this.urlapidata();

        const myagent = new https.Agent({  
            rejectUnauthorized: false
        });

        var senddata = await axios({
            method      : req.method,
            url         : urldata+req.url,
            data        : req.data,
            httpsAgent  : myagent,
        }).then(res => {

            return {
                    status  : res.status,
                    data    : res.data
                }

            // return res.status

        })
        .catch(function (error) {
            return error.message;
        });
    
        // console.log(data);
        return senddata;
    }

    async axiosheader(req,headerdata){

        var urldata = await this.urlapidata();

        const myagent = new https.Agent({  
            rejectUnauthorized: false
        });

        var data = await axios({
            method      : req.method,
            url         : urldata+req.url,
            data        : req.data,
            httpsAgent  : myagent,
            headers     : headerdata,
        }).then(res => {
            // console.log(res.data);
            // return res.data;
            return {
                status  : res.status,
                data    : res.data
            }
        })
        .catch(function (error) {
            return error.message;
        });
    
        // console.log(data);
        return data;
    }

    
}

module.exports = AxioController
