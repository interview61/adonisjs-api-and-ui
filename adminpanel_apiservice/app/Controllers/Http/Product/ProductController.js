'use strict'
const Axioscontrol      = use('App/Controllers/Http/Axios/AxioController')
const axiosctrl         = new Axioscontrol()
const fs                = use('fs')

class ProductController {

  
  async index ({ request, response, view,auth,session }) {
     
    session.put('menu','Product')
    
    return view.render('product.index',{ session })
  }

  async dataproduct ({ request, response, view,auth,session }) {
      const requestall  = request.all()

        var requp = {
            method  : 'POST',
            url     : 'productservice/listdata',
            data    : {
                limit     : requestall.length,
                offset    : requestall.start,
                search    : requestall.search.value,
                orderclm  : parseInt(requestall.order[0].column),
                sort      : requestall.order[0].dir,
            }
        } 

        var header = {
            'Authorization': 'Bearer '+session.get('bearer')
        }
        
      var datasend = await axiosctrl.axiosheader(requp,header);

      var urlapi = await axiosctrl.urlapidata()

      var datapush = []
      for(let i in datasend.data.qdata){
        let items       = datasend.data.qdata[i]
        let idrows      = items.id

        let deletedata  = "deletedata('"+idrows+"')"

        let editdata  = "editdata('"+idrows+"','"+items.nama_barang+"','"+items.harga_beli+"','"+items.harga_jual+"','"+items.stok+"')"

      
          var action = '<div class="dropdown">'
                +'<span class="btn btn-primary btn-sm dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                    +'Select'
                +'</span>'
            
                +'<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">'
                  +'<a class="dropdown-item" onclick="'+editdata+'">'
                    +'Edit'
                  +'</a>'
                  +'<a class="dropdown-item" href="#" onclick="'+deletedata+'">'
                    +'Delete'
                  +'</a>'

                +'</div>'
            +'</div>'

        
        var image = '<img src="'+urlapi+items.foto_barang+'">';

        datapush.push({
          // foto        : image,
          namabarang  : items.nama_barang,
          hargabeli   : items.harga_beli.toLocaleString(),
          hargajual   : items.harga_jual.toLocaleString(),
          stok        : items.stok.toLocaleString(),
          action      : action
        })
      }

      var responseup = {
        "draw"                : requestall.draw,
        "iTotalRecords"       : datasend.data.qdatacount.total,
        "iTotalDisplayRecords": datasend.data.qdatacount.total,
        "aaData"              : datapush
      };

      return response.json(responseup)  
  }

  async create ({ request, response, view }) {
  }

  async store ({ request, response, view,auth,session }) {


    const requestall  = request.all()

        var requp = {
            method  : 'POST',
            url     : 'productservice',
            data    : {
                foto_barang : '',//request.file('foto_barang'),//fotodidalam,//fotonya,
                nama_barang : requestall.nama_barang,
                harga_beli  : requestall.harga_beli,
                harga_jual  : requestall.harga_jual,
                stok        : requestall.stok,
            }
        } 
        
        var header = {
            'Authorization' : 'Bearer '+session.get('bearer'),
            // "Content-Type"  : 'multipart/form-data',
        }
        
      var datasend = await axiosctrl.axiosheader(requp,header);

    return {
      status  : datasend.status,
      data    : datasend.data.message
    }
  }

  async show ({ params, request, response, view }) {
  }

  async edit ({ params, request, response, view }) {
  }

  async update ({ params, request, response ,session}) {
    var id = params.id

    const requestall  = request.all()

    var requp = {
            method  : 'PUT',
            url     : 'productservice/'+id,
            data    : {
                foto_barang : '',//request.file('foto_barang'),//fotodidalam,//fotonya,
                nama_barang : requestall.nama_barang,
                harga_beli  : requestall.harga_beli,
                harga_jual  : requestall.harga_jual,
                stok        : requestall.stok,
            }
        } 
        
        var header = {
            'Authorization' : 'Bearer '+session.get('bearer'),
            // "Content-Type"  : 'multipart/form-data',
        }
        
      var datasend = await axiosctrl.axiosheader(requp,header);
  
    return {
      status  : datasend.status,
      data    : datasend.data.message
    }
  }

  async destroy ({ params, request, response, session }) {
    var id = params.id

    const requestall  = request.all()

    var requp = {
            method  : 'DELETE',
            url     : 'productservice/'+id,
            data    : {
                foto_barang : '',//request.file('foto_barang'),//fotodidalam,//fotonya,
                nama_barang : requestall.nama_barang,
                harga_beli  : requestall.harga_beli,
                harga_jual  : requestall.harga_jual,
                stok        : requestall.stok,
            }
        } 
        
        var header = {
            'Authorization' : 'Bearer '+session.get('bearer'),
            // "Content-Type"  : 'multipart/form-data',
        }
        
      var datasend = await axiosctrl.axiosheader(requp,header);
  
    return {
      status  : datasend.status,
      data    : datasend.data.message
    }
  }
}

module.exports = ProductController
