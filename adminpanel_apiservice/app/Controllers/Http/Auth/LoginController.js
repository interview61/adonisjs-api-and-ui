'use strict'

const { validate }      = use('Validator')

const Axioscontrol      = use('App/Controllers/Http/Axios/AxioController')
const axiosctrl         = new Axioscontrol()

class LoginController {

    async index ({ request, response, view,auth,session }) {
        if(session.get('bearer')){
            return response.redirect('/adminpanel/welcome')
        }else{
            return view.render('auth.login',{ session })
        }
    }

    async login({ request, auth, session, response },next) {

        const rules = { 
            email       : 'required',
            password    : 'required',
        }
        const nicename =  {
            'email.required' :'Required',     attribute : 'email',
            'password.required' :'Required',     attribute : 'password',
        }
        
        const validation = await validate(request.all(), rules,nicename)

        if (validation.fails()) {
            session.withErrors( validation.messages() ).flashAll()
            session.flash({notification: 'Email Or Password Required' })
            return response.redirect('/adminpanel')
        }else{
            var requp = {
                method  : 'POST',
                url     : 'login',
                data    : {
                    email       : request.input('email'),
                    password    : request.input('password'),
                }
            } 
            var axios = await axiosctrl.axiosnoheader(requp);
            

            if(axios.status == '200'){

                session.put('bearer',axios.data.token.token)
                session.put('user',axios.data.data)
                session.put('baseurl','/adminpanel/')

                session.flash({notification: axios.data.message })
              
                return response.redirect('/adminpanel')
            }else{
              
                session.flash({notification: 'Username Or Password Wrong' })
                session.clear()
                return response.redirect('/adminpanel')
            }
        }
    }

    async logout({ auth, response,request,session }) {
        
        var requp = {
            method  : 'GET',
            url     : 'logout',
            data    : {
                bearer : session.get('bearer'),
            }
        } 

        var header = {
            'Authorization': 'Bearer '+session.get('bearer')
        }
        
        await axiosctrl.axiosheader(requp,header);

        // return tesdata
        session.clear()

        session.flash({notification: 'Logout Success' })
        return response.redirect('/adminpanel')
    }
}

module.exports = LoginController
