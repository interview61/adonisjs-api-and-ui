'use strict'

class TemplateController {
  
  async noakses({ request, response, auth, view }) {
    // const menu    = await menuhelper.menuuser(auth,request,response,'view')
    const dtarray = {
      menulist: ''
    }

    return view.render('template.noakses',{ dtarray })
  }

  async index ({ request, response, auth, view,session }) {

    session.put('menu','')
    return view.render('template.welcome',{ session })
  }

  async create ({ request, response, view }) {
  }

  async store ({ request, response }) {
  }

  async show ({ params, request, response, view }) {
  }

  async edit ({ params, request, response, view }) {
  }

  async update ({ params, request, response }) {
  }

  async destroy ({ params, request, response }) {
  }
}

module.exports = TemplateController
