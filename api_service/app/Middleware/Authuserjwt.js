'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const User = use('App/Models/User')
const Token = use('App/Models/Token')

class Authuserjwt {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request,auth,response }, next) {
    // call next to advance the request

    if(auth.user){
      
      if(auth.user.is_revoked == 0 && auth.user.remember_token == auth.getAuthHeader()){
        await next()
      }else{
        return response.status(401).json({
          message : 'Unauthorized'
        })
      }
      
    }else{
      const urlmenu     = request.url().split("/")[3]
      if(urlmenu == 'login'){
        await next()
      }else{
        return response.status(401).json({
          message : 'Unauthorized'
        })
      }

    }
  }
}

module.exports = Authuserjwt
