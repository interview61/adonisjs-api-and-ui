'use strict'

const User = use('App/Models/User')
const Token = use('App/Models/Token')

class AuthuserController {

    async login({request, auth, response}) {
        let input = request.all()
       
        let token = await auth.withRefreshToken().attempt(input.email, input.password);
        if(token){

            var updatedata = await User.updateOrCreate(
                {
                    email     : input.email, 
                },
                {
                    remember_token  : token.token,
                    is_revoked      : 0
                });
           
            if(updatedata.active == 1){
                return response.status(200).json({
                    token   : token,
                    data    : updatedata,
                    message : 'Success'
                });
            }else{

                //----------------
                await User.updateOrCreate(
                    {
                        id     : updatedata.id, 
                    },
                    {
                        is_revoked      : 1,
                        remember_token  : null
                    });
                    

                await Token.query().where('user_id',updatedata.id).update({
                    is_revoked : 1
                })
                //----------------
                return response.status(401).json({
                    message : 'User Not Active'
                });
            }

           
        }else{
            return response.status(401).json({
                message : 'Unauthorized'
            });
        }
        
    }

    async getuser({request, auth, response}) {
        try{
            return response.status(200).json({
                token   : auth.getAuthHeader(),
                data    : auth.user,
                message : 'Success'
            });
        }catch(e){
            return response.status(204).json({
                message : 'Failed'
            });
        }
    }

    async logout({request, auth, response}) {

        const user = await User.find(auth.user.id)

        await User.query().where('id',auth.user.id).update({
            is_revoked      : 1,
            remember_token  : null
        })

        await auth
        .authenticator('jwt')
        .revokeTokensForUser(user)

        return response.status(200).json({
            message : 'Logout Success'
        });
    }
}

module.exports = AuthuserController
