'use strict'

const { validate }    = use('Validator')
const Database        = use('Database')
const fs              = use('fs')

const User      = use('App/Models/User')
const Token     = use('App/Models/Token')
const Product   = use('App/Models/Product/Product')
const mProduct  = new Product();

class ProductController {
 
  async index ({ request, response, view,auth }) {

    return response.status(200).json({
      data  : auth.user
    })
  }

  async listdata({request, auth, response}) {
    var reqall = request.all()
   
    var qdata = await mProduct.listdataprod(reqall);

    return response.status(200).json(qdata)
        
  }
  
  async create ({ request, response, view }) {
  }

  async cekextfile(file){
    var extarray  = ['jpeg','jpg','JPG','JPEG','png','PNG']
    var max       = 100000

    var fotoext   = file.extname
    var fotosize  = parseInt(file.size)

    var cekext = extarray.includes(fotoext)
    if(cekext){
      if(fotosize <= max){

        if(fs.existsSync('public/myapps/api/product/'+file.clientName)) {

          var usefoto = 'N'
          var message = 'File Exist'
          var image   = '';

        }else{
          var usefoto = 'Y'
          var message = 'File Dapat Di Gunakan'
          var image   = 'product/'+file.clientName;
        }

        
      }else{
        var usefoto = 'N'
        var message = 'Max File Size 100kb'
        var image   = '';
      }
    }else{
      var usefoto = 'N'
      var message = 'Extention File (jpeg,jpg,png)'
      var image   = '';
    } 

    return {
      usefoto : usefoto,
      message : message,
      image   : image,
    }
  }

  async store ({ request, response ,view,auth}) {
    
    const rules = { 
      nama_barang    : 'required|max:200|unique:products',
      harga_beli     : 'required|integer|min:0',
      harga_jual     : 'required|integer|min:0',
      stok           : 'required|integer|min:0',
    }

    const nicename =  {
      'nama_barang.required' :'Nama Barang Di Perlukan', attribute : 'nama_barang',
      'nama_barang.max'      :'Nama Barang Max 200 Character', attribute : 'nama_barang',
      'nama_barang.unique'   :'Nama Barang Tidak Boleh Sama', attribute : 'nama_barang',

      'harga_beli.required'  :'Harga Beli Di Perlukan', attribute : 'harga_beli',
      'harga_beli.integer'   :'Harga Beli Harus Angka', attribute : 'harga_beli',
      'harga_beli.min'       :'Harga Beli Min 0', attribute : 'harga_beli',

      'harga_jual.required'  :'Harga Jual Di Perlukan', attribute : 'harga_jual',
      'harga_jual.integer'   :'Harga Jual Harus Angka', attribute : 'harga_jual',
      'harga_jual.min'       :'Harga Jual Min 0', attribute : 'harga_jual',

      'stok.required'  :'Stok Di Perlukan', attribute : 'stok',
      'stok.integer'   :'Stok Harus Angka', attribute : 'stok',
      'stok.min'       :'Stok Min 0', attribute : 'stok',
    }
  
    if(request.file('foto_barang')){
      
      var fotonya  = request.file('foto_barang');
      var rulefoto = await this.cekextfile(fotonya);

      if(rulefoto.usefoto == 'Y'){
        //-----------------------------------------------------------------
       
        const validation = await validate(request.all(), rules,nicename)
    
        if (validation.fails()) {
            // session.withErrors( validation.messages() ).flashAll()
            
            return response.status('201').json({
              message : validation.messages()[0].message
            })
    
        }else{
    
    
          const trx = await Database.beginTransaction()
            try {
              
              await fotonya.move('public/myapps/api/product/')

              await mProduct.store(request,rulefoto.image,'0')
    
              await trx.commit()
    
              return response.status('200').json({
                message : 'Success'
              })
    
            } catch (error) {
              await trx.rollback()
    
              return response.status('201').json({
                message : error
              })
            }
    
          
        }
        //-----------------------------------------------------------------
      }else{
        return response.status('201').json({
          message : rulefoto.message
        })
      }
    }else{
      // return response.status('201').json({
      //   message : 'Foto Barang Di Butuhkan'
      // })

      //-----------------------------------------------------------------
       
      const validation = await validate(request.all(), rules,nicename)
    
      if (validation.fails()) {
          // session.withErrors( validation.messages() ).flashAll()
          
          return response.status('201').json({
            message : validation.messages()[0].message
          })
  
      }else{
  
  
        const trx = await Database.beginTransaction()
          try {
  
            await mProduct.store(request,'','0')
  
            await trx.commit()
  
            return response.status('200').json({
              message : 'Success'
            })
  
          } catch (error) {
            await trx.rollback()
  
            return response.status('201').json({
              message : error
            })
          }
  
        
      }
      //-----------------------------------------------------------------
    } 
    

  }

  async show ({ params, request, response, view }) {
  }

  async edit ({ params, request, response, view }) {
  }


  async updatestoredata(request,dataheader,unlinkyn,id,rulefoto){

    var fotonya  = request.file('foto_barang');
    
    //-------------------------------------------------
    const rules = { 
      nama_barang    : 'required|max:200',
      harga_beli     : 'required|integer|min:0',
      harga_jual     : 'required|integer|min:0',
      stok           : 'required|integer|min:0',
    }

    const nicename =  {
      'nama_barang.required' :'Nama Barang Di Perlukan', attribute : 'nama_barang',
      'nama_barang.max'      :'Nama Barang Max 200 Character', attribute : 'nama_barang',
      // 'nama_barang.unique'   :'Nama Barang Tidak Boleh Sama', attribute : 'nama_barang',

      'harga_beli.required'  :'Harga Beli Di Perlukan', attribute : 'harga_beli',
      'harga_beli.integer'   :'Harga Beli Harus Angka', attribute : 'harga_beli',
      'harga_beli.min'       :'Harga Beli Min 0', attribute : 'harga_beli',

      'harga_jual.required'  :'Harga Jual Di Perlukan', attribute : 'harga_jual',
      'harga_jual.integer'   :'Harga Jual Harus Angka', attribute : 'harga_jual',
      'harga_jual.min'       :'Harga Jual Min 0', attribute : 'harga_jual',

      'stok.required'  :'Stok Di Perlukan', attribute : 'stok',
      'stok.integer'   :'Stok Harus Angka', attribute : 'stok',
      'stok.min'       :'Stok Min 0', attribute : 'stok',
    }

    const validation = await validate(request.all(), rules,nicename)

    //-----------------------------------------------------------------

                if (validation.fails()) {

                    var status  = 201;
                    var message = validation.messages()[0].message;

                }else{

                  if(request.input('nama_barang') == dataheader.nama_barang){
                    const trx = await Database.beginTransaction()
                    try {
                      if(unlinkyn == 'Y'){
                        fs.unlink('public/myapps/api/'+dataheader.foto_barang, function(err) {
                    
                        });
                        await fotonya.move('public/myapps/api/product/')
  
                        await mProduct.store(request,rulefoto.image,id)
                      }else{
                        await mProduct.store(request,dataheader.foto_barang,id)
                      }
                      
                      await trx.commit()

                      var status  = 200;
                      var message = 'Success';

                    } catch (error) {
                      await trx.rollback()

                      var status  = 201;
                      var message = error;

                    }
                  }else{
                    var ceksama = await Product.findBy('nama_barang',request.input('nama_barang'))
                    if(ceksama){
                      var status  = 201;
                      var message = 'Nama Barang Tidak Boleh Sama';
                    }else{
                          const trx = await Database.beginTransaction()
                          try {
                            if(unlinkyn == 'Y'){
                              fs.unlink('public/myapps/api/'+dataheader.foto_barang, function(err) {
                          
                              });
                              await fotonya.move('public/myapps/api/product/')
        
                              await mProduct.store(request,rulefoto.image,id)
                            }else{
                              await mProduct.store(request,dataheader.foto_barang,id)
                            }
                            
                            await trx.commit()
      
                            var status  = 200;
                            var message = 'Success';
      
                          } catch (error) {
                            await trx.rollback()
      
                            var status  = 201;
                            var message = error;
      
                          }
                    } 
                  }

                  
                }
                //-----------------------------------------------------------------

      return {
        status  : status,
        message : message
      }

  }
  
  async update ({params, request, response ,view,auth}) {

    var id = params.id

    var dataheader = await Product.findBy('id',id)

    
    if(request.file('foto_barang')){
            var fotonya  = request.file('foto_barang');
            var rulefoto = await this.cekextfile(fotonya);

      if(rulefoto.usefoto == 'Y'){

        var updata      = await this.updatestoredata(request,dataheader,'Y',id,rulefoto)
        var statusnya   = updata.status
        var messagenya  = updata.message

      }else{
        var statusnya   = 201
        var messagenya  = rulefoto.message
      }

    }else{
      var updata      = await this.updatestoredata(request,dataheader,'N',id,'')
      var statusnya   = updata.status
      var messagenya  = updata.message
    }

    return response.status(statusnya).json({
      message : messagenya
    })

    
  }

  async destroy ({ params, request, response }) {
    var id = params.id

    var dataheader = await Product.findBy('id',id)

    const trx = await Database.beginTransaction()
    try {
      
      await mProduct.deletedata(id)

      if(fs.existsSync('public/myapps/api/'+dataheader.foto_barang)) {
        fs.unlink('public/myapps/api/'+dataheader.foto_barang, function(err) {
                    
        });
      }
      
      await trx.commit()

      return response.status(200).json({
        message : 'Success'
      })

    } catch (error) {
      await trx.rollback()

      return response.status(201).json({
        message : error
      })

    }
  }
}

module.exports = ProductController
