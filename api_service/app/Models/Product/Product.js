'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

const Carbon    = use("carbonjs");

class Product extends Model {
    static boot () {
        super.boot()
    
        this.addTrait("@provider:Lucid/UpdateOrCreate")
    }

    async listdataprod(requestall){

        const limit     = requestall.limit
        const offset    = requestall.offset
        const search    = requestall.search
        const orderclm  = parseInt(requestall.orderclm)
        const sort      = requestall.sort
    
        const arrayclm = [
              'nama_barang',
              'harga_beli',
              'harga_jual',
              'stok',
              'id',
              'foto_barang',
            ]
    
        const mqdata    = await Product.query()
            .orderBy(arrayclm[orderclm],sort)
            .where(function(searchdata){
              searchdata
              .where(arrayclm[0], 'like', '%' + search + '%')
              .orWhere(arrayclm[1], 'like', '%' + search + '%')
              .orWhere(arrayclm[2], 'like', '%' + search + '%')
              .orWhere(arrayclm[3], 'like', '%' + search + '%')
              .orWhere(arrayclm[4], 'like', '%' + search + '%')
            })
            .select(arrayclm)
            .limit(limit)
            .offset(offset)
            .fetch()
    
        const mqdatacount = await Product.query()
              .where(function(searchdata){
                searchdata
                .where(arrayclm[0], 'like', '%' + search + '%')
                .orWhere(arrayclm[1], 'like', '%' + search + '%')
                .orWhere(arrayclm[2], 'like', '%' + search + '%')
                .orWhere(arrayclm[3], 'like', '%' + search + '%')
                .orWhere(arrayclm[4], 'like', '%' + search + '%')
              })
              .count(`${arrayclm[5]}`+' as total') 
              .first()
       
        return {qdata:mqdata,qdatacount:mqdatacount}
    }

    async store(request,image,id){
        if(id == '0'){
            await Product.updateOrCreate(
                {
                    nama_barang     : request.input('nama_barang'), 
                },
                {
                    foto_barang     : image,
                    harga_beli      : request.input('harga_beli'), 
                    harga_jual      : request.input('harga_jual'), 
                    stok            : request.input('stok'), 
                });
        }else{ 
            await Product.updateOrCreate(
                {
                    id              : id
                },
                {
                    nama_barang     : request.input('nama_barang'), 
                    foto_barang     : image,
                    harga_beli      : request.input('harga_beli'), 
                    harga_jual      : request.input('harga_jual'), 
                    stok            : request.input('stok'), 
                });
        }
        

        return true;
    }

    async deletedata(id){
        await Product.query().where('id',id).delete()
        
        return true
    }
}

module.exports = Product
