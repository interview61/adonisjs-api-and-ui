'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { message: 'Welcome' }
})

Route.get('/myapps/api/', () => {
  return { message: 'Welcome' }
})

Route.post('/myapps/api/login', 'Auth/AuthuserController.login')

Route.group(() => {

  Route.get('/getuser', 'Auth/AuthuserController.getuser')
  Route.get('/logout', 'Auth/AuthuserController.logout')

  Route.post('/productservice/listdata', 'Product/ProductController.listdata')
  Route.resource('/productservice', 'Product/ProductController')

}).middleware(['Authuserjwt']).prefix('/myapps/api/');
// }).prefix('/myapps/api/');
