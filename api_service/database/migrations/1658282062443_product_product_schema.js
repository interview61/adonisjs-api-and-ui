'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductSchema extends Schema {
  up () {
    this.create('products', (table) => {
      table.bigIncrements()
      table.text('foto_barang').nullable()
      table.string('nama_barang').nullable().unique()
      table.integer('harga_beli').nullable()
      table.integer('harga_jual').nullable()
      table.integer('stok').nullable()
      
      table.timestamps()
    })
  }

  down () {
    this.drop('products')
  }
}

module.exports = ProductSchema
