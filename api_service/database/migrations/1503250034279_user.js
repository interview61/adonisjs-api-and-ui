'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('fullname').nullable()
      table.string('username', 255).notNullable().unique()
      table.string('email', 254).notNullable().unique()
      table.text('password').notNullable()
      table.integer('active').unsigned()
      table.boolean('is_revoked').defaultTo(false)
      table.text('remember_token').nullable()
      
      table.timestamp('deleted_at', { useTz: true }).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
