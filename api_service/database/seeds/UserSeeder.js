'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

const modeldata = use('App/Models/User')

class UserSeeder {
  async run () {

    await modeldata.updateOrCreate(
      {
        username    : 'admin',
        email       : 'admin@gmail.com'
      },
      { 
        fullname    : 'admin',
        password    : '123123' ,
        active      : 1,
        is_revoked  : 1,
      }
    );

    await modeldata.updateOrCreate(
      {
        username    : 'ujang',
        email       : 'ujang@gmail.com'
      },
      { 
        fullname    : 'ujang',
        password    : '123123' ,
        active      : 0,
        is_revoked  : 1,
      }
    );

  }
}

module.exports = UserSeeder
